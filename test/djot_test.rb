require "test_helper"

class DjotTest < TestCase
  test "VERSION" do
    assert do
      ::Djot.const_defined?(:VERSION)
    end
  end

  test "render_html" do
    assert_respond_to(Djot, :render_html)
  end

  test "render_ast" do
    assert_respond_to(Djot, :render_ast)
  end

  class FunctionTest < TestCase
    Case = Struct.new(:ticks, :modifier, :input, :output,
                      keyword_init: true)

    TARGET_PROCESSORS = [Djot::JavaScript].freeze

    TARGET_PROCESSORS.each do |processor|
      Dir["#{__dir__}/../vendor/djot.js/test/*"].each do |path|
        state = :ignore
        current_case = Case.new(input: "", output: "")
        index = 0
        File.foreach(path) do |line|
          line.force_encoding(Encoding::UTF_8)
          case state
          when :ignore
            next unless (match = line.match(/\A(?<ticks>`+)\s*(?<modifier>.+)?\n\Z/))

            current_case.ticks = match[:ticks]
            current_case.modifier = (match[:modifier] || :html).intern
            state = :input
          when :input
            unless line == ".\n"
              current_case.input += line
              next
            end

            state = :output
          when :output
            unless line.match?(/\A#{current_case.ticks}\n\Z/)
              current_case.output += line
              next
            end

            input = current_case.input
            output = current_case.output
            modifier = current_case.modifier

            next if processor == Djot::Pure

            next if processor == Djot::JavaScript && (
                      (File.basename(path) == "attributes.test" &&
                        [6, 7, 31].include?(index)
                      ) ||
                      (File.basename(path) == "emphasis.test" &&
                        [10].include?(index)
                      ) ||
                      (File.basename(path) == "filters.test" &&
                        [0, 1, 2].include?(index)
                      ) ||
                      (File.basename(path) == "lists.test" &&
                        [27].include?(index)
                      ) ||
                      File.basename(path) == "smart.test"
                    )

            test "#{processor.name} > #{File.basename(path)} > #{index} (modifier: #{modifier})" do
              actual = case modifier
                       when :a
                         processor.render_ast(processor.parse(input))
                       when :ap
                         processor.render_ast(processor.parse(input, source_positions: true), source_positions: true)
                       else
                         processor.render_html(processor.parse(input))
                       end
              assert_equal(output, actual)
            end

            current_case.input = ""
            current_case.output = ""
            current_case.ticks = nil
            current_case.modifier = nil
            index += 1
            state = :ignore
          end
        end
      end
    end
  end
end
