FROM ruby
ARG WORKDIR=/app
RUN mkdir $WORKDIR
WORKDIR $WORKDIR
COPY djot.gemspec ./djot.gemspec
COPY lib/djot/version.rb ./lib/djot/version.rb
COPY Gemfile ./Gemfile
RUN bundle -j$(nproc)
COPY . .
RUN rake
