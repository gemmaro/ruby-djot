require_relative "djot/version"
require_relative "djot/javascript"
require_relative "djot/pure"

# Provides Djot parsing functionalities.
# +Djot.*+ methods are pointing to JavaScript implementation now.
# See also Djot::JavaScript for ones pointing to JavaScript implementation.
module Djot
  class Error < StandardError; end

  def self.render_html(doc, warn: nil)
    JavaScript.render_html(doc, warn: warn)
  end

  def self.render_ast(input, source_positions: false)
    JavaScript.render_ast(input, source_positions: source_positions)
  end
end
